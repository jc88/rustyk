var searchIndex = JSON.parse('{\
"docs":{"doc":"","t":[5,5],"n":["add","foo"],"q":["docs",""],"d":["","foo is a function"],"i":[0,0],"f":[[[1,1],1],[[]]],"p":[[15,"usize"]]},\
"lib":{"doc":"","t":[5],"n":["add"],"q":["lib"],"d":[""],"i":[0],"f":[[[1,1],1]],"p":[[15,"usize"]]}\
}');
if (typeof window !== 'undefined' && window.initSearch) {window.initSearch(searchIndex)};
if (typeof exports !== 'undefined') {exports.searchIndex = searchIndex};
