use std::collections::HashMap;

fn main() {
    println!("Hello, world!");
    vectors();

    all_about_string();

    _about_hash_map();
}

fn vectors() {
    // initialize vector
    let v: Vec<i32> = Vec::new();
    println!("Result [v] of initialize way 1: {:?}", v);

    let v = vec![1, 2, 3];
    println!("Result [v] of initialize way 1: {:?}", v);

    // change vector
    let mut v1 = Vec::new();

    v1.push(5);
    v1.push(6);
    v1.push(7);
    v1.push(8);

    println!("Result [v1] after changes 1: {:?}", v1);

    // delete
    {
        let mut v = vec![1, 2, 3, 4];

        v.remove(1);
        v.pop();
        println!("Result [v] after delete action: {:?}", v);

        // do stuff with v
    } // <-

    // Reading Elements of Vectors
    let v = vec![1, 2, 3, 4, 5];
    let third: &i32 = &v[2];
    println!("The third element is {}", third);

    match v.get(2) {
        Some(third2) => println!("The third element is {}", third2),
        None => println!("There is no third element."),
    }

    // Iterating over the Values in a Vector
    let v = vec![100, 32, 57];

    println!("Start iterating");

    for i in &v {
        println!("{}", i);
    }

    println!("Finish iterating");

    iterate_with_changes();

    // Using an Enum to Store Multiple Types
    use_enum();

    examples()
}

fn iterate_with_changes() {
    println!("Start iterating");

    let mut v = vec![100, 32, 57];
    for i in &mut v {
        *i += 50;
    }

    println!("Finish iterating");

    println!("after changes {:?}", v)
}

fn use_enum() {
    enum SpreadsheetCell {
        Int(i32),
        Float(f64),
        Text(String),
    }

    let _row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];

    // println!("{:?}", row)
}

fn examples() {
    let mut vec = Vec::new();
    vec.push(1);
    vec.push(2);

    assert_eq!(vec.len(), 2);
    assert_eq!(vec[0], 1);

    assert_eq!(vec.pop(), Some(2));
    assert_eq!(vec.len(), 1);

    vec[0] = 7;
    assert_eq!(vec[0], 7);

    vec.extend([1, 2, 3].iter().copied());

    for x in &vec {
        println!("{}", x);
    }
    assert_eq!(vec, [7, 1, 2, 3]);

    let mut vec1 = vec![1, 2, 3];
    vec1.push(4);
    let vec2 = Vec::from([1, 2, 3, 4]);
    assert_eq!(vec1, vec2);

    let vec = vec![0; 5];
    assert_eq!(vec, [0, 0, 0, 0, 0]);

    // The following is equivalent, but potentially slower:
    let mut vec = Vec::with_capacity(5);
    vec.resize(5, 0);
    assert_eq!(vec, [0, 0, 0, 0, 0]);
}

fn all_about_string() {
    #[warn(unused_mut)]
    let mut s = String::new();
    println!("{}", s);

    about_string()
}

fn about_string() {
    let data = "initial contents";

    let s = data.to_string();

    // the method also works on a literal directly:
    let s = "initial contents".to_string();
    println!("{}", s);
    {
        let mut s = String::from("foo");
        s.push_str("bar");
        assert_eq!(s, "foobar");
    }

    {
        let s1 = String::from("tic");
        let s2 = String::from("tac");
        let s3 = String::from("toe");

        let s = format!("{}-{}-{}", s1, s2, s3);

        assert_eq!(s, "tic-tac-toe");
    }

    {
        let hello = "Здравствуйте";

        let s = &hello[0..4];

        println!("{}", s)
    }

    {
        for c in "नमस्ते".chars() {
            println!("{}", c);
        }
    }
}

fn _about_hash_map() {
    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    println!("{:?}", scores);

    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 50];

    let mut scores2: HashMap<_, _> = teams.into_iter().zip(initial_scores.into_iter()).collect();
    println!("{:?}", scores2);

    // Hash Maps and Ownership

    let field_name = String::from("Favorite color");
    let field_value = String::from("Blue");

    let mut map = HashMap::new();
    map.insert(field_name, field_value);
    // field_name and field_value are invalid at this point, try using them and
    // see what compiler error you get!
    // println!("{}", field_name) !!! error

    hash_get();
    overwriting();
    overwriting_values();
}

// Accessing Values in a Hash Map
fn hash_get() {
    let mut scores = HashMap::new();

    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    let team_name = String::from("Blue");
    let score = scores.get(&team_name);
    println!("{:?}", score);

    for (key, value) in &scores {
        println!("{}: {}", key, value);
    }
}

fn overwriting() {
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);

    scores.entry(String::from("Yellow")).or_insert(50);
    scores.entry(String::from("Blue")).or_insert(50);

    println!("{:?}", scores);
}

// Updating a Value Based on the Old Value
fn overwriting_values() {
    let text = "hello world wonderful world";

    let mut map = HashMap::new();

    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }

    println!("{:?}", map);
}
